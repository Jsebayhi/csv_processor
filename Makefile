########
# MAIN #
########

# DOC
#####

# Override
##########
MAKEFILE_SOURCES = makefile_sources

# Const
#######
CMD_PATHS = ./cmd/processor

##########
# IMPORT #
##########
include $(MAKEFILE_SOURCES)/common.lib.mk

#########
# RULES #
#########

.DEFAULT_GOAL := help

.PHONY: cmd_build
cmd_build: ## build all cmds
	@for cmd_path in $(CMD_PATHS) ; do \
		echo "building $$cmd_path" ;\
		make build -C $$cmd_path ;\
	done

.PHONY: cmd_re
cmd_re: ## rebuild all cmds
	@for cmd_path in $(CMD_PATHS) ; do \
		echo "rebuilding $$cmd_path" ;\
		make re -C $$cmd_path ;\
	done

.PHONY: fclean
fclean: ## clean all cleanable things of the project
	@for cmd_path in $(CMD_PATHS) ; do \
		echo "cleaning $$cmd_path" ;\
		make fclean -C $$cmd_path ;\
	done