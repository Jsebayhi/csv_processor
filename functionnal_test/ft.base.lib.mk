###########
# LIBRARY #
###########

# DOC
#####

# REQUIRED
##########
#-PROJECT_PATH
#-BIN_PATH
#-MAKEFILE_SOURCES

# Optional
##########

# Const
#######
PROJECT_EXECUTABLE_NAME=csv_processor
TEST_ROOT=test_area
SETUP_ROOT=setup

##########
# IMPORT #
##########
include $(MAKEFILE_SOURCES)/common.lib.mk

#########
# RULES #
#########

all: test_prepare project_prepare run  ## setup the test, build and fetch the project's executable then run the test

run: ## run the test
	cd ./$(TEST_ROOT) && ./$(PROJECT_EXECUTABLE_NAME) -configurationPath "conf.yaml"

clean: project_clean test_clean ## clean the project and the test

re: clean all ## clean the rerun the test

# project 
#--------

project_prepare: project_build get_project_executable ## build the project then fetch the executable to be tested

project_build: ## rebuild the project
	make re -C $(PROJECT_PATH)

project_clean: ## clean the project as well as the fetched project's executable
	make clean -C $(PROJECT_PATH)
	rm -rf $(PROJECT_EXECUTABLE_NAME)

get_project_executable: ## fetch the project's executable
	rm -rf $(TEST_ROOT)/$(PROJECT_EXECUTABLE_NAME)
	cp $(BIN_PATH)/$(PROJECT_EXECUTABLE_NAME) ./$(TEST_ROOT)/$(PROJECT_EXECUTABLE_NAME)

# test setup
#-----------

.PHONY: test_reset
test_reset: test_clean test_prepare ## clean then set up tests


.PHONY: test_prepare
test_prepare: test_prepare_env test_prepare_fixtures ## setup everything necessary in order to run the test

.PHONY: test_clean
test_clean: ## clean every test related file
	rm -rf $(TEST_ROOT)

.PHONY: test_prepare_env
test_prepare_env: ## prepare the test environnement such as folders
	mkdir $(TEST_ROOT)
	mkdir $(TEST_ROOT)/files
	mkdir -p $(TEST_ROOT)/files/raw
	mkdir -p $(TEST_ROOT)/files/processed
	mkdir -p $(TEST_ROOT)/files/computed

.PHONY: test_prepare_fixtures
test_prepare_fixtures: ## copy fixtures to the test environnement
	cp -r $(SETUP_ROOT)/files $(TEST_ROOT)/
	cp -r $(SETUP_ROOT)/root/* $(TEST_ROOT)/
