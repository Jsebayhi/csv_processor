#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

###################
# COMMON VARIABLE #
###################

SCRIPT_NAME=$(basename "$0")
SCRIPT_PATH=$(dirname "$0")

#########
# USAGE #
#########

function usage() {
	echo ""
	echo "Usage: ${SCRIPT_NAME} [OPTION]"
	echo ""
	echo "Mandatory options:"
	echo "-p: Processed path."
	echo "-r: Raw path."
	echo "Available options:"
	echo "-d: Print commands as they are executed."
	echo "-h: Help. Print usage."
	echo "-v: Print commands as they are executed without looking interpreting variables content."
	echo 
}

####################
# OPTIONS HANDLING #
####################

while getopts "dhp:r:s:v" option
do
	case $option in
			
		d)
			set -o xtrace
			;;
		h)
			usage
			exit 0
			;;
		p)
			PROCESSED_PATH=${OPTARG:-}
			;;
		r)
			RAW_PATH=${OPTARG:-}
			;;
		v)
			set -o verbose
			;;

		\?)
			usage
			exit 1
			;;
			
	esac
done

function unspecified_mandatory_option() {
	unspecified_mandatory_option_OPTION=$1
	echo "Mandatory option unspecified: -${unspecified_mandatory_option_OPTION}." 1>&2
	MANDATORY_OPTIONS_STATUS="KO"
}

if [ -z ${RAW_PATH:-} ]; then
	unspecified_mandatory_option "r"
fi

if [ -z ${PROCESSED_PATH:-} ]; then
	unspecified_mandatory_option "p"
fi

if [ -n "${MANDATORY_OPTIONS_STATUS:-}" ]; then
	usage
	exit 1
fi

##########################
# CONSTANTES DEFINITIONS #
##########################

###################
# INPUT CONSTANTE #
###################

######################
# EXECUTION HANDLING #
######################

RES=$(diff ${RAW_PATH}/ ${PROCESSED_PATH}/)
if [ ! -z ${RES:-} ];then
	echo "Test failed."
	exit 1
fi