// + build unit

package condition

import "errors"

var mockedError = errors.New("mocked error")

type csvSchemaMock struct {
	ValidateFieldsResults []error
	ValidateFieldResults  error
}

func (mock csvSchemaMock) ValidateFields(candidates []fieldName) []error {
	return mock.ValidateFieldsResults
}
func (mock csvSchemaMock) ValidateField(candidates fieldName) error {
	return mock.ValidateFieldResults
}

type inputStub struct {
	m map[string]string
}

func (in inputStub) Get(f fieldName) (fieldContent, error) {
	return in.m[f], nil
}
