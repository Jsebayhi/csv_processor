package condition

type Not struct {
	condition Condition
}

func (c Not) ValidateInputSchema(sch csvSchema) error {
	return c.condition.ValidateInputSchema(sch)
}

func (c Not) Apply(in input) (bool, error) {
	res, err := c.condition.Apply(in)
	return !res, err
}
