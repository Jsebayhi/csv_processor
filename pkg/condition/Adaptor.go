package condition

type csvSchema = interface {
	ValidateFields([]fieldName) []error
	ValidateField(fieldName) error
}

type input = interface {
	Get(fieldName) (fieldContent, error)
}

type fieldName = string
type fieldContent = string
