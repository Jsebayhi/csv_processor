package condition

import (
	"fmt"
)

type Or struct {
	//TODO: validation that there is at least one condition
	Conditions []Condition //[1; +inf[
}

func (c Or) ValidateInputSchema(schema csvSchema) error {
	for _, cond := range c.Conditions {
		if err := cond.ValidateInputSchema(schema); err != nil {
			return newIncompatibleInputSchemaError(schema, c, "Sub Combinaison was incorrect.", err)
		}
	}
	return nil
}

func (c Or) Apply(in input) (bool, error) {
	for i, cond := range c.Conditions {
		if res, err := cond.Apply(in); err != nil {
			return false, fmt.Errorf("when applying the condition %d contained in the Or condition, %v", i, err)
		} else if res {
			return res, nil
		}
	}
	return false, nil
}
