//build unit

package condition

// ValidateInputSchema stubs

type invalidInputSchemaCondition struct{}

func (c invalidInputSchemaCondition) ValidateInputSchema(sch csvSchema) error {
	return incompatibleInputSchemaError{
		Schema:    sch,
		Condition: c,
	}
}

func (c invalidInputSchemaCondition) Apply(in input) (bool, error) {
	return true, nil
}

type validInputSchemaCondition struct{}

func (c validInputSchemaCondition) ValidateInputSchema(sch csvSchema) error {
	return nil
}

func (c validInputSchemaCondition) Apply(in input) (bool, error) {
	return true, nil
}

// Apply stub

type trueCondition struct{}

func (c trueCondition) ValidateInputSchema(sch csvSchema) error {
	return nil
}

func (c trueCondition) Apply(in input) (bool, error) {
	return true, nil
}

type falseCondition struct{}

func (c falseCondition) ValidateInputSchema(sch csvSchema) error {
	return nil
}

func (c falseCondition) Apply(in input) (bool, error) {
	return false, nil
}
