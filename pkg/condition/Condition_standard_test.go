// +build unit

package condition

import (
	"testing"
)

type testCase_Apply struct {
	conditionTree Condition
	line          map[fieldName]fieldContent
	expect        bool
	expectError   bool
}

func (tc testCase_Apply) Run(tt *testing.T, testDesc string) {
	tt.Run(testDesc, func(t *testing.T) {
		res, resErr := tc.conditionTree.Apply(inputStub{m: tc.line})
		if tc.expect != res {
			t.Errorf("given conditionTree '%v'"+
				" and line '%v',"+
				" expected '%v',"+
				" got '%v'",
				tc.conditionTree, tc.line, tc.expect, res)
		}
		if (tc.expectError && resErr == nil) || (!tc.expectError && resErr != nil) {
			t.Errorf("given conditionTree '%v'"+
				" and line '%v',"+
				" expected error? '%v',"+
				" got error ? '%v'",
				tc.conditionTree, tc.line, tc.expectError, resErr)
		}
	})
}

type testCase_ValidateInputSchema struct {
	conditionTree Condition
	schema        csvSchemaMock
	expect        bool
}

func (tc testCase_ValidateInputSchema) Run(t *testing.T, testDesc string) {
	t.Run(testDesc, func(t *testing.T) {
		var res = tc.conditionTree.ValidateInputSchema(tc.schema)
		if !(tc.expect && res == nil) && !(!tc.expect && res != nil) {
			t.Errorf("given conditionTree '%v'"+
				" and schema '%v'"+
				" expected '%v',"+
				" got '%v'",
				tc.conditionTree, tc.schema, tc.expect, res == nil)
		}
	})
}
