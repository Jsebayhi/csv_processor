package condition

// The common interface satisfied by all implemented conditions
// TODO: make it package private to forbid package interdependance by design
type Condition interface {
	Apply(input) (bool, error)
	ValidateInputSchema(csvSchema) error
}
