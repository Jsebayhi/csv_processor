package condition

type FieldEqual struct {
	FieldName string
	RefValue  string
}

func (c FieldEqual) ValidateInputSchema(sch csvSchema) error {
	if err := sch.ValidateField(c.FieldName); err != nil {
		return newIncompatibleInputSchemaError(sch, c, "Invalid schema.", err)
	}
	return nil
}

func (c FieldEqual) Apply(in input) (bool, error) {
	v, err := in.Get(c.FieldName)
	return c.RefValue == v, err
}
