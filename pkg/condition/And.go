package condition

import "fmt"

type And struct {
	//TODO: validation that there is at least one condition
	Conditions []Condition //[1; +inf[
}

func (c And) ValidateInputSchema(schema csvSchema) error {
	for _, cond := range c.Conditions {
		if err := cond.ValidateInputSchema(schema); err != nil {
			return newIncompatibleInputSchemaError(schema, c, "Sub Combinaison was incorrect.", err)
		}
	}
	return nil
}

func (c And) Apply(in input) (bool, error) {
	last := true
	for i, cond := range c.Conditions {
		res, err := cond.Apply(in)
		if err != nil {
			return last, fmt.Errorf("when applying the condition %d contained in the And condition, %v", i, err)
		}
		last = last && res
		if !last {
			return false, nil
		}
	}
	return last, nil
}
