// +build unit

package condition

import (
	"testing"
)

func TestFieldEqual_Apply(t *testing.T) {
	for testDesc, tc := range map[string]testCase_Apply{
		"should find the reference in the given line": {
			conditionTree: FieldEqual{FieldName: "field1", RefValue: "content1"},
			line:          map[string]string{"field1": "content1", "field2": "content2"},
			expect:        true,
			expectError:   false,
		},
		"should not find the reference in the line": {
			conditionTree: FieldEqual{FieldName: "field1", RefValue: "wontFindIt"},
			line:          map[string]string{"field1": "content1", "field2": "content2"},
			expect:        false,
			expectError:   false,
		},
		"should not find the reference in the line if looking for a content from the field2 in the field1": {
			conditionTree: FieldEqual{FieldName: "field1", RefValue: "content2"},
			line:          map[string]string{"field1": "content1", "field2": "content2"},
			expect:        false,
			expectError:   false,
		},
	} {
		tc.Run(t, testDesc)
	}
}

func TestFieldEqual_CheckInputSchema(t *testing.T) {
	for testDesc, tc := range map[string]testCase_ValidateInputSchema{
		"should be compatible with the specified schema": {
			conditionTree: FieldEqual{FieldName: "field2", RefValue: "irrelevantForTheTest"},
			schema: csvSchemaMock{
				ValidateFieldsResults: []error{},
				ValidateFieldResults:  nil,
			}, expect: true,
		},
		"should not be compatible with the specified schema": {
			conditionTree: FieldEqual{FieldName: "unknown Field", RefValue: "irrelevantForTheTest"},
			schema: csvSchemaMock{
				ValidateFieldsResults: []error{mockedError},
				ValidateFieldResults:  mockedError,
			},
			expect: false,
		},
	} {
		tc.Run(t, testDesc)
	}
}
