package condition

import (
	"fmt"
)

func newIncompatibleInputSchemaError(sch csvSchema, condition Condition, desc string, cause error) incompatibleInputSchemaError {
	return incompatibleInputSchemaError{
		Schema:    sch,
		Condition: condition,
		Desc:      desc,
		Cause:     cause,
	}
}

type incompatibleInputSchemaError struct {
	Schema    csvSchema
	Condition Condition
	Desc      string
	Cause     error
}

func (e incompatibleInputSchemaError) Error() string {
	return fmt.Sprintf("The provided input schema is not compatible with the action. "+e.Desc) +
		fmt.Sprintf(" schema: %v, action: %v.", e.Schema, e.Condition) +
		fmt.Sprintf(" Cause: %v", e.Cause)
}
