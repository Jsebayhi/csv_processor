// +build unit

package condition

import (
	"testing"
)

func TestAnd_Apply(t *testing.T) {
	for testDesc, tc := range map[string]testCase_Apply{
		"when containing one valid Condition": {
			conditionTree: And{
				Conditions: []Condition{
					trueCondition{},
				},
			},
			expect:      true,
			expectError: false,
		},
		"when containing one invalid Condition": {
			conditionTree: And{
				Conditions: []Condition{
					falseCondition{},
				},
			},
			expect:      false,
			expectError: false,
		},
		"when containing multiple valid Conditions": {
			conditionTree: And{
				Conditions: []Condition{
					trueCondition{},
					trueCondition{},
					trueCondition{},
				},
			},
			expect:      true,
			expectError: false,
		},
		"when containing one invalid Condition among multiple valid Conditions": {
			conditionTree: And{
				Conditions: []Condition{
					trueCondition{},
					trueCondition{},
					falseCondition{},
				},
			},
			expect:      false,
			expectError: false,
		},
	} {
		tc.Run(t, testDesc)
	}
}

func TestAnd_IsInputSchemaCompatible(t *testing.T) {
	for testDesc, tc := range map[string]testCase_ValidateInputSchema{
		"when containing a Condition containing a compatible schema field": {
			conditionTree: And{
				Conditions: []Condition{
					validInputSchemaCondition{},
				},
			},
			expect: true,
		},
		"when containing a Condition containing an incompatible schema field": {
			conditionTree: And{
				Conditions: []Condition{
					invalidInputSchemaCondition{},
				},
			},
			expect: false,
		},
		"when containing multiple Conditions containing compatible schema fields": {
			conditionTree: And{
				Conditions: []Condition{
					validInputSchemaCondition{},
					validInputSchemaCondition{},
					validInputSchemaCondition{},
				},
			},
			expect: true,
		},
		"when containing a condition with an incompatible field among multiple Conditions containing compatible schema fields": {
			conditionTree: And{
				Conditions: []Condition{
					validInputSchemaCondition{},
					invalidInputSchemaCondition{},
					validInputSchemaCondition{},
				},
			},
			expect: false,
		},
	} {
		tc.Run(t, testDesc)
	}
}
