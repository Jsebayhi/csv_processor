package csv

import (
	"encoding/csv"
	"fmt"
	"io"
	"io/ioutil"
	"strings"
)

type Reader = interface {
	Read(path string) ([][]string, error)
}

// Implementation

func NewReader(fs fileSettings) Reader {
	return reader{settings: fs}
}

type reader struct {
	settings fileSettings
}

func (r reader) Read(path string) ([][]string, error) {
	fileContent, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, fmt.Errorf("when reading csv file, %v", err)
	}
	return r.read(string(fileContent))
}

func (cr reader) read(content string) (records [][]string, err error) {
	r := csv.NewReader(strings.NewReader(content))

	r.Comma = cr.settings.GetSeparator()

	for {
		record, err := r.Read()
		if err == io.EOF {
			break
		}
		if record == nil && err != nil {
			return nil, fmt.Errorf("when reading csv content, %v", err)
		}
		records = append(records, record)
	}
	return records, nil
}
