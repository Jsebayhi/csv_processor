package csv

type fileSettings interface {
	GetSeparator() rune
}

// Implementations

type FileSettings struct {
	Separator rune
}

func (fs FileSettings) GetSeparator() rune {
	return fs.Separator
}
