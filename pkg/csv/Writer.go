package csv

import (
	"encoding/csv"
	"io"
	"os"
)

type Writer = interface {
	Write(path string, records [][]string) error
}

// Implementations

func NewWriter(fs fileSettings) Writer {
	return writer{settings: fs}
}

type writer struct {
	settings fileSettings
}

func (w writer) Write(path string, records [][]string) error {
	f, err := os.OpenFile(path, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return err
	}
	defer f.Close()
	return w.write(f, records)
}

func (cw writer) write(fileWriter io.Writer, records [][]string) (err error) {
	w := csv.NewWriter(fileWriter)

	w.Comma = cw.settings.GetSeparator()

	for _, record := range records {
		if err := w.Write(record); err != nil {
			return err
		}
	}

	w.Flush()

	return w.Error()
}
