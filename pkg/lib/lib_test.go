// +build unit-tests

package lib

import "testing"

func TestStringToRune(t *testing.T) {
	{
		input := "tooLong"
		var expected rune = ' ' //default incorrect returned rune
		expectedError := StringToRuneInvalidStringError(input)

		res, err := StringToRune(input)
		if err.Error() != expectedError.Error() {
			t.Errorf("given '%v', expected error '%v' but got '%v'.", input, expectedError, err)
		}
		if res != expected {
			t.Errorf("given '%v', expected result '%v', got '%v'", input, expected, res)
		}
	}
	{
		input := "t"
		var expected rune = 't'
		var expectedError error = nil

		res, err := StringToRune(input)
		if err != expectedError {
			t.Errorf("given '%v', expected error '%v' but got '%v'.", input, expectedError, err)
		}
		if res != expected {
			t.Errorf("given '%v', expected result '%v', got '%v'", input, expected, res)
		}
	}
}
