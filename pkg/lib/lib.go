package lib

/*
*** String list manipulation
**/

func ApplyStringList(strLst []string, f func(string) string) (strLstNew []string) {
	for _, str := range strLst {
		strLstNew = append(strLstNew, f(str))
	}
	return
}

func ApplyStringList1(strLst []string, f func(string)) {
	for _, str := range strLst {
		f(str)
	}
}
