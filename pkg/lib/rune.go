package lib

import "errors"

//Unicode Character 'ZERO WIDTH NO-BREAK SPACE'
const ZeroWidthNoBreakSpace = rune(65279)

func StringToRune(str string) (rune, error) {
	if len(str) != 1 {
		return ' ', StringToRuneInvalidStringError(str)
	}
	return []rune(str)[0], nil
}

func StringToRuneInvalidStringError(str string) error {
	return errors.New("invalid string to run convertion, string contain more than 1 caracter: " + str)
}
