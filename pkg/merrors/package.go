// more errors
package merrors

func EnrichErrors(vs []error, f func(error) error) []error {
	vsm := make([]error, len(vs))
	for i, v := range vs {
		vsm[i] = f(v)
	}
	return vsm
}
