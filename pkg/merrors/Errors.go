package merrors

import "fmt"

type Errors interface {
	Build() error
	Enrich() []error
	DoesContainErrors() bool
}

type ErrorsBuilder interface {
	Build() error
	Enrich() []error
	DoesContainErrors() bool
	Append(...error)
}

func NewErrorsBuilder(desc string) ErrorsBuilder {
	return &errorsBuilder{
		desc: desc,
	}
}

type errorsBuilder struct {
	desc   string
	errors []error
}

func (e errorsBuilder) Build() error {
	if e.DoesContainErrors() {
		return fmt.Errorf("%s, found errors: %v", e.desc, e.errors)
	} else {
		return nil
	}
}

func (e errorsBuilder) Enrich() []error {
	return EnrichErrors(e.errors, func(err error) error {
		return fmt.Errorf("%s, %v", e.desc, err)
	})
}

func (e errorsBuilder) DoesContainErrors() bool {
	return len(e.errors) != 0
}

func (e *errorsBuilder) Append(err ...error) {
	e.errors = append(e.errors, err...)
}
