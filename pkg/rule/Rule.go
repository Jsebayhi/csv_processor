package rule

// The common interface satisfied by all implemented rules
// TODO: make it package private to forbid package interdependance by design
type Rule interface {
	Apply(processedUnit) (processedUnit, error)
	IsApplicable(processedUnit) (bool, error)
	ValidateInputSchema(csvSchema) error
	ValidateOutputSchema(csvSchema) error
}
