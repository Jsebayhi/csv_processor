package rule

type Common struct {
	Condition condition
	Action    action
}

//TODO: add context to error
func (r Common) IsApplicable(pu processedUnit) (bool, error) {
	return r.Condition.Apply(pu.GetInput())
}

//TODO: add context to error
func (r Common) Apply(pu processedUnit) (processedUnit, error) {
	return r.Action.Apply(pu)
}

func (r Common) ValidateInputSchema(sch csvSchema) error {
	if err := r.Condition.ValidateInputSchema(sch); err != nil {
		return newIncompatibleInputSchemaError(sch, r, "Invalid condition.", err)
	}
	if err := r.Action.ValidateInputSchema(sch); err != nil {
		return newIncompatibleInputSchemaError(sch, r, "Invalid action.", err)
	}
	return nil
}

func (r Common) ValidateOutputSchema(sch csvSchema) error {
	if err := r.Action.ValidateOutputSchema(sch); err != nil {
		return newIncompatibleOutputSchemaError(sch, r, "Invalid action.", err)
	}
	return nil
}
