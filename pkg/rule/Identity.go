package rule

type Identity struct{}

func (r Identity) IsApplicable(pu processedUnit) bool {
	return true
}

func (r Identity) Apply(pu processedUnit) processedUnit {
	pu.SetOutput(pu.GetInput())
	return pu
}

func (r Identity) ValidateInputSchema(sch csvSchema) error {
	return nil
}

func (r Identity) ValidateOutputSchema(sch csvSchema) error {
	return nil
}
