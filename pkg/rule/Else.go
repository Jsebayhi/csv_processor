package rule

import (
	"strconv"
)

// Check if a rule is applicable, if not, check if the following is
// applicable and return the result of the first applicable rule found.
type Else struct {
	Rules []Rule
}

// Return true. This rule is always applicable since it is a rule trying to
// apply a list of Rule.
func (r Else) IsApplicable(pu processedUnit) (bool, error) {
	return true, nil
}

func (r Else) Apply(pu processedUnit) (processedUnit, error) {
	for i, rule := range r.Rules {
		isApplicable, err := r.IsApplicable(pu)
		if err != nil {
			return pu, err
		}
		if isApplicable {
			newPU, err := rule.Apply(pu)
			if err != nil {
				return pu, applyError{
					ProcessedUnit: pu,
					Rule:          r,
					Desc:          "An error occured when applying action " + strconv.Itoa(i) + ".",
					Cause:         err,
				}
			}
			return newPU, nil
		}
	}
	return pu, nil
}

func (r Else) ValidateInputSchema(sch csvSchema) error {
	for i, r := range r.Rules {
		if err := r.ValidateInputSchema(sch); err != nil {
			return newIncompatibleInputSchemaError(sch, r, "Invlaid condition"+strconv.Itoa(i)+".", err)
		}
		if err := r.ValidateInputSchema(sch); err != nil {
			return newIncompatibleInputSchemaError(sch, r, "Invlaid action"+strconv.Itoa(i)+".", err)
		}
	}
	return nil
}

func (r Else) ValidateOutputSchema(sch csvSchema) error {
	for i, r := range r.Rules {
		if err := r.ValidateOutputSchema(sch); err != nil {
			return newIncompatibleOutputSchemaError(sch, r, "Invlaid action"+strconv.Itoa(i)+".", err)
		}
	}
	return nil
}
