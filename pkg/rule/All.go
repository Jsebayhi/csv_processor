package rule

import (
	"strconv"
)

type All struct {
	Rules []Rule
}

func (r All) IsApplicable(pu processedUnit) (bool, error) {
	return true, nil
}

//TODO: add context to error
func (r All) Apply(pu processedUnit) (updatedPU processedUnit, err error) {
	updatedPU = pu
	for _, rule := range r.Rules {
		if updatedPU, err = rule.Apply(updatedPU); err != nil {
			return
		}
	}
	return
}

func (r All) ValidateInputSchema(sch csvSchema) error {
	for i, rule := range r.Rules {
		if err := rule.ValidateInputSchema(sch); err != nil {
			return newIncompatibleInputSchemaError(sch, r, "when checking action "+strconv.Itoa(i)+".", err)
		}
	}
	return nil
}

func (r All) ValidateOutputSchema(sch csvSchema) error {
	for i, rule := range r.Rules {
		if err := rule.ValidateOutputSchema(sch); err != nil {
			return newIncompatibleOutputSchemaError(sch, r, "when checking action "+strconv.Itoa(i)+".", err)
		}
	}
	return nil
}
