package rule

import "bitbucket.org/Jsebayhi/csv_processor/pkg/merrors"

type action = interface {
	Apply(processedUnit) (processedUnit, error)
	ValidateInputSchema(csvSchema) error
	ValidateOutputSchema(csvSchema) error
}

type condition = interface {
	Apply(input) (bool, error)
	ValidateInputSchema(csvSchema) error
}

type processedUnit = interface {
	GetInput() unit
	GetOutput() unit
	SetOutput(unit) error
	RegisterError(error)
	Error() error
}

type unit = interface {
	Set(string, string) error
	Get(string) (string, error)
}

type input = interface {
	Get(string) (string, error)
}

type csvSchema = interface {
	ValidateFields([]fieldName) merrors.Errors
	ValidateField(fieldName) error
}

type fieldName = string
type fieldContent = string
