package rule

type AlwaysAct struct {
	Action action
}

func (r AlwaysAct) IsApplicable(pu processedUnit) (bool, error) {
	return true, nil
}

//TODO: add context to error
func (r AlwaysAct) Apply(pu processedUnit) (processedUnit, error) {
	return r.Action.Apply(pu)
}

func (r AlwaysAct) ValidateInputSchema(sch csvSchema) error {
	if err := r.Action.ValidateInputSchema(sch); err != nil {
		return newIncompatibleInputSchemaError(sch, r, "Invlaid action.", err)
	}
	return nil
}

func (r AlwaysAct) ValidateOutputSchema(sch csvSchema) error {
	if err := r.Action.ValidateOutputSchema(sch); err != nil {
		return newIncompatibleOutputSchemaError(sch, r, "Invlaid action.", err)
	}
	return nil
}
