package rule

import (
	"fmt"
)

type applyError struct {
	Rule          Rule
	ProcessedUnit processedUnit
	Desc          string
	Cause         error
}

func (e applyError) Error() string {
	return "The application of the action failed. " + e.Desc +
		fmt.Sprintf("rule: %v, processedUnit: %v. Cause: %v.", e.Rule, e.ProcessedUnit, e.Cause)
}

type incompatibleInputSchemaError = incompatibleSchemaError

func newIncompatibleInputSchemaError(sch csvSchema, rule Rule, desc string, cause error) incompatibleInputSchemaError {
	return incompatibleInputSchemaError{
		Schema:  sch,
		Rule:    rule,
		SchKind: "input",
		Desc:    desc,
		Cause:   cause,
	}
}

type incompatibleOutputSchemaError = incompatibleSchemaError

func newIncompatibleOutputSchemaError(sch csvSchema, rule Rule, desc string, cause error) incompatibleOutputSchemaError {
	return incompatibleOutputSchemaError{
		Schema:  sch,
		Rule:    rule,
		SchKind: "output",
		Desc:    desc,
		Cause:   cause,
	}
}

type incompatibleSchemaError struct {
	Schema  csvSchema
	Rule    Rule
	SchKind string
	Desc    string
	Cause   error
}

func (e incompatibleSchemaError) Error() string {
	return fmt.Sprintf("The provided %s schema is not compatible with the action. %s", e.SchKind, e.Desc) +
		fmt.Sprintf(" schema: %v, rule: %v.", e.Schema, e.Rule) +
		fmt.Sprintf(" Cause: %v", e.Cause)
}
