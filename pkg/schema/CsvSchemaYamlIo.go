package schema

import (
	"fmt"
	"strings"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/lib"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/yaml"
)

//IO
func LoadCsvSchemaFromYamlFile(path string) (CsvSchema, error) {
	scf, err := loadCsvSchemaYaml(path)
	if err != nil {
		return nil, err
	}
	return fromCsvSchemaYaml(scf)
}

type csvSchemaYaml struct {
	Uid       string   `yaml:"uid"`
	Separator string   `yaml:"separator"`
	Fields    []string `yaml:"fields"`
}

// IO
func loadCsvSchemaYaml(path string) (scf csvSchemaYaml, err error) {
	err = yaml.LoadYamlFile(path, &scf)
	return scf, err
}

func fromCsvSchemaYaml(scf csvSchemaYaml) (CsvSchema, error) {
	separator, err := lib.StringToRune(scf.Separator)
	if err != nil {
		return nil, fmt.Errorf("invalid csvSchemaYaml - separator is not a rune: %v", scf.Separator)
	}
	csvS := csvSchema{
		Uid:       scf.Uid,
		Fields:    lib.ApplyStringList(scf.Fields, strings.TrimSpace),
		Separator: separator,
	}
	return csvS, nil
}
