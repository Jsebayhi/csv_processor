package schema

import (
	"bitbucket.org/Jsebayhi/csv_processor/pkg/merrors"
)

//TODO: make package private
type FieldName = string

//TODO: make package private
type FieldContent = string

type CsvSchema interface {
	GetUid() string
	GetSeparator() rune
	GetFields() []FieldName
	GetFieldPosition(FieldName) (int, error)
	ValidateFields([]FieldName) merrors.Errors
	ValidateField(FieldName) error
	DoesContainFields([]FieldName) bool
	DoesContainField(FieldName) bool
}

//TODO: add field position indication
type csvSchema struct {
	Uid       string
	Separator rune
	Fields    []FieldName
}

func (s csvSchema) GetUid() string {
	return s.Uid
}

func (s csvSchema) GetSeparator() rune {
	return s.Separator
}

func (s csvSchema) GetFields() []FieldName {
	return s.Fields
}

func (s csvSchema) GetFieldPosition(f FieldName) (int, error) {
	for i, field := range s.Fields {
		if field == f {
			return i, nil
		}
	}
	return -1, NewUnknownSchemaFieldName(f)
}

// Call DoesContainField in order to go through the schema's fields once since
// candidates' size is unknown.
func (s csvSchema) DoesContainFields(candidates []FieldName) bool {
	for _, candidate := range candidates {
		if !s.DoesContainField(candidate) {
			return false
		}
	}
	return true
}

func (s csvSchema) DoesContainField(candidate FieldName) bool {
	for _, field := range s.Fields {
		if field == candidate {
			return true
		}
	}
	return false
}

func (s csvSchema) ValidateFields(candidates []FieldName) merrors.Errors {
	errorBuilder := merrors.NewErrorsBuilder("at least one field is invalid")
	for _, candidate := range candidates {
		if err := s.ValidateField(candidate); err != nil {
			errorBuilder.Append(err)
		}
	}
	return errorBuilder
}

func (s csvSchema) ValidateField(candidate FieldName) error {
	for _, field := range s.Fields {
		if field == candidate {
			return nil
		}
	}
	return NewUnknownSchemaFieldName(candidate)
}
