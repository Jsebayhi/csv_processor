package schema

import "fmt"

type UnknownSchemaFieldName = error

func NewUnknownSchemaFieldName(f FieldName) error {
	return fmt.Errorf("schema does not contain " + f)
}
