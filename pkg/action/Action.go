package action

// The common interface satisfied by all implemented actions
// TODO: make it package private to forbid package interdependance by design
type Action interface {
	Apply(processedUnit) (processedUnit, error)
	ValidateInputSchema(csvSchema) error
	ValidateOutputSchema(csvSchema) error
}
