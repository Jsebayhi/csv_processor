package action

type Identity struct{}

func (a Identity) Apply(pu processedUnit) (processedUnit, error) {
	return pu, nil
}

func (a Identity) ValidateInputSchema(sch csvSchema) error {
	return nil
}

func (a Identity) ValidateOutputSchema(sch csvSchema) error {
	return nil
}
