// +build example

package action

// processedUnit example
// Every calcul are independant from the previous one.
type IndependantProcessedUnitExample struct {
	Input  map[fieldName]fieldContent
	Output map[fieldName]fieldContent
}

func (pu IndependantProcessedUnitExample) GetInput() map[fieldName]fieldContent {
	return pu.Input
}
func (pu IndependantProcessedUnitExample) GetOutput() map[fieldName]fieldContent {
	return pu.Output
}
func (pu *IndependantProcessedUnitExample) UpdateOutput(update map[fieldName]fieldContent) error {
	pu.Output = update
	return nil
}

func NewIndependantProcessedUnitExample(input map[fieldName]fieldContent) processedUnit {
	return &IndependantProcessedUnitExample{
		Input:  input,
		Output: input,
	}
}

// processedUnit example
// Every calcul are made on the result of the previous one.
type DependantProcessedUnitExample struct {
	state map[fieldName]fieldContent
}

func (pu DependantProcessedUnitExample) GetInput() map[fieldName]fieldContent {
	return pu.state
}
func (pu DependantProcessedUnitExample) GetOutput() map[fieldName]fieldContent {
	return pu.state
}
func (pu *DependantProcessedUnitExample) UpdateOutput(update map[fieldName]fieldContent) error {
	pu.state = update
	return nil
}

func NewDependantProcessedUnitExample(input map[fieldName]fieldContent) processedUnit {
	return &DependantProcessedUnitExample{
		state: input,
	}
}
