package action

// Mapping with one mapping per reference
type OneToOneMapping struct {
	Mapping       map[string]string
	SrcColumnName string
	DstColumnName string
}

//TODO: add context to error
func (a OneToOneMapping) Apply(pu processedUnit) (processedUnit, error) {
	output := pu.GetOutput()

	key, err := pu.GetInput().Get(a.SrcColumnName)
	if err != nil {
		return pu, err
	}

	mapResult, ok := a.Mapping[key]
	if !ok {
		return pu, nil
	}

	output.Set(a.DstColumnName, mapResult)
	if err := pu.SetOutput(output); err != nil {
		return pu, applyError{
			Action: a,
			Input:  pu.GetInput(),
			Cause:  err,
		}
	}
	return pu, nil
}

func (a OneToOneMapping) ValidateInputSchema(sch csvSchema) error {
	if err := sch.ValidateField(a.SrcColumnName); err != nil {
		return newIncompatibleInputSchemaError(sch, a, "Schema not compatible.", err)
	}
	return nil
}

func (a OneToOneMapping) ValidateOutputSchema(sch csvSchema) error {
	if err := sch.ValidateField(a.DstColumnName); err != nil {
		return newIncompatibleOutputSchemaError(sch, a, "Schema not compatible.", err)
	}
	return nil
}
