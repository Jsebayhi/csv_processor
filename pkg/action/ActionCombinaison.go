package action

import (
	"strconv"
)

// Apply actions in order.
type ActionCombination struct {
	actions []Action
}

func (a ActionCombination) Apply(pu processedUnit) (updatedPU processedUnit, err error) {
	updatedPU = pu
	for i, action := range a.actions {
		if updatedPU, err = action.Apply(updatedPU); err != nil {
			return pu, applyError{
				Action: a,
				Input:  pu.GetInput(),
				Cause:  err,
				Desc:   "When executing action number " + strconv.Itoa(i) + ".",
			}
		}
	}
	return updatedPU, nil
}

func (a ActionCombination) ValidateInputSchema(sch csvSchema) error {
	for i, action := range a.actions {
		if err := action.ValidateInputSchema(sch); err != nil {
			return newIncompatibleInputSchemaError(sch, a, "when checking action "+strconv.Itoa(i)+".", err)
		}
	}
	return nil
}

func (a ActionCombination) ValidateOutputSchema(sch csvSchema) error {
	for i, action := range a.actions {
		if err := action.ValidateOutputSchema(sch); err != nil {
			return newIncompatibleOutputSchemaError(sch, a, "when checking action "+strconv.Itoa(i)+".", err)
		}
	}
	return nil
}
