package action

import (
	"fmt"
)

type incompatibleInputSchemaError = incompatibleSchemaError

func newIncompatibleInputSchemaError(sch csvSchema, action Action, desc string, cause error) incompatibleInputSchemaError {
	return incompatibleInputSchemaError{
		Schema:  sch,
		Action:  action,
		SchKind: "input",
		Desc:    desc,
		Cause:   cause,
	}
}

type incompatibleOutputSchemaError = incompatibleSchemaError

func newIncompatibleOutputSchemaError(sch csvSchema, action Action, desc string, cause error) incompatibleOutputSchemaError {
	return incompatibleOutputSchemaError{
		Schema:  sch,
		Action:  action,
		SchKind: "output",
		Desc:    desc,
		Cause:   cause,
	}
}

type incompatibleSchemaError struct {
	Schema  csvSchema
	Action  Action
	SchKind string
	Desc    string
	Cause   error
}

func (e incompatibleSchemaError) Error() string {
	return fmt.Sprintf("The provided %s schema is not compatible with the action. %s", e.SchKind, e.Desc) +
		fmt.Sprintf(" schema: %v, action: %v.", e.Schema, e.Action) +
		fmt.Sprintf(" Cause: %v", e.Cause)
}

type applyError struct {
	Action Action
	Input  unit
	Desc   string
	Cause  error
}

func (e applyError) Error() string {
	return "The application of the action failed. " + e.Desc +
		fmt.Sprintf("action: %v, input: %v. Cause: %v.", e.Action, e.Input, e.Cause)
}
