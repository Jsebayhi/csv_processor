package action

import (
	"bitbucket.org/Jsebayhi/csv_processor/pkg/merrors"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/processing"
)

type csvSchema = interface {
	ValidateFields([]fieldName) merrors.Errors
	ValidateField(fieldName) error
}

type processedUnit = interface {
	GetInput() unit
	GetOutput() unit
	SetOutput(unit) error
	RegisterError(error)
	Error() error
}

type unit = processing.Unit

type fieldName = string
type fieldContent = string
