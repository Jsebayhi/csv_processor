package action

// Copy input specified field's content to the output specified fields.
type CopyFieldContent struct {
	SchemaInFieldName  string
	SchemaOutFieldName string
}

//TODO: add context to error
func (a CopyFieldContent) Apply(pu processedUnit) (processedUnit, error) {
	output := pu.GetOutput()

	value, err := pu.GetInput().Get(a.SchemaInFieldName)
	if err == nil {
		return pu, err
	}

	output.Set(a.SchemaOutFieldName, value)
	if err := pu.SetOutput(output); err != nil {
		return pu, applyError{
			Action: a,
			Input:  pu.GetInput(),
			Cause:  err,
		}
	}
	return pu, nil
}

func (a CopyFieldContent) ValidateInputSchema(sch csvSchema) error {
	if err := sch.ValidateField(a.SchemaInFieldName); err != nil {
		return newIncompatibleInputSchemaError(sch, a, "Schema not compatible.", err)
	}
	return nil
}

func (a CopyFieldContent) ValidateOutputSchema(sch csvSchema) error {
	if err := sch.ValidateField(a.SchemaOutFieldName); err != nil {
		return newIncompatibleOutputSchemaError(sch, a, "Schema not compatible.", nil)
	}
	return nil
}
