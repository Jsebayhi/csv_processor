package processing

import "fmt"

type Unit = interface {
	Set(fieldName, string) error
	Get(fieldName) (string, error)
	DoesContain(fieldName) bool
}

// Implementations

func NewStringValuesUnit() Unit {
	return &stringValuesUnit{
		ctt: map[fieldName]string{},
	}
}

type stringValuesUnit struct {
	ctt map[fieldName]string
}

func (o *stringValuesUnit) Set(field string, value string) error {
	o.ctt[field] = value
	return nil
}

func (o stringValuesUnit) Get(field string) (string, error) {
	maybeValue, ok := o.ctt[field]
	if !ok {
		return "", fmt.Errorf("unknwon field %s", field)
	}
	return maybeValue, nil
}

func (u stringValuesUnit) DoesContain(f fieldName) bool {
	_, doesContain := u.ctt[f]
	return doesContain
}
