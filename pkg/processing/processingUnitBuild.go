package processing

import (
	"fmt"
)

type processingUnitBuilder = interface {
	Build([]string) (ProcessingUnit, error)
}

type processingUnitUnBuilder = interface {
	Unbuild(ProcessingUnit) (row []string, rowErr error, err error)
}

// Standard implementation

type StdProcessedUnitBuilder struct {
	UnitBuilder         UnitBuilder
	BuildProcessingUnit ProcessingUnitBuilder
}

func (b StdProcessedUnitBuilder) Build(row []string) (ProcessingUnit, error) {
	u, err := b.UnitBuilder.Build(row)
	if err != nil {
		return nil, fmt.Errorf("when creating processingUnit from row, %v", err)
	}
	pu := b.BuildProcessingUnit(u)
	if err != nil {
		return pu, fmt.Errorf("when creating processingUnit from row, %v", err)
	}
	return pu, nil
}

type StdProcessedUnitUnBuilder struct {
	UnitUnBuilder UnitUnBuilder
}

func (b StdProcessedUnitUnBuilder) UnBuild(pu ProcessingUnit) (row []string, rowErr error, err error) {
	rowErr = pu.Error()
	if rowErr != nil {
		return nil, rowErr, nil
	}
	row, err = b.UnitUnBuilder.Unbuild(pu.GetOutput())
	if err != nil {
		return nil, nil, fmt.Errorf("when extracting output datas, %v", err)
	}
	return row, rowErr, nil
}
