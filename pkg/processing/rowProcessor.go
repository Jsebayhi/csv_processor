package processing

import (
	"fmt"
	"strings"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/lib"
)

type RowProcessor interface {
	// return (row, rowError, processingFatalError)
	Process([]string) ([]string, error, error)
}

// Implementations

// Row processor which process the row as a header.
type CsvHeaderProcessor struct {
	InputSch  schemaValidator
	OutputSch schemaHeaderBuilder
}

func (hp CsvHeaderProcessor) Process(h []string) ([]string, error, error) {
	cleanedHeader := hp.cleanHeader(h)

	err := hp.validateHeader(cleanedHeader)
	if err != nil {
		return nil, nil, fmt.Errorf("during header processing, %v", err)
	}
	return hp.buildNewHeader(), nil, nil
}

func (hp CsvHeaderProcessor) cleanHeader(h []string) []string {
	return append([]string{hp.cleanContent(h[0])}, h[1:]...)
}

func (hp CsvHeaderProcessor) cleanContent(c string) string {
	return strings.Trim(c, string(lib.ZeroWidthNoBreakSpace))
}

func (hp CsvHeaderProcessor) validateHeader(h []string) error {
	if err := hp.InputSch.ValidateFields(h).Build(); err != nil {
		return fmt.Errorf("invalid header, %v", err)
	}
	return nil
}

func (hp CsvHeaderProcessor) buildNewHeader() []string {
	return hp.OutputSch.GetFields()
}

// Row processor which allow to apply multiple row processor one after another
// on a row.
type CsvRowChainedProcessor struct {
	processors []RowProcessor
}

func (rp CsvRowChainedProcessor) Process(row []string) (res []string, rowErr error, err error) {
	res = row
	for i, processor := range rp.processors {
		in := res
		res, rowErr, err = processor.Process(in)
		if err != nil {
			return nil, nil, fmt.Errorf("an error occured during processing %d,"+
				" last row valid state was %v,"+
				" it ended in this state: %v "+
				"with the following error: %v", i, in, res, err)
		} else if rowErr != nil {
			return nil, fmt.Errorf("after processing %d, %v", i, rowErr), nil
		}
	}
	return
}

// Row processor which transform a row.
type CsvRowProcessor struct {
	PuBuilder   processedUnitBuilder
	PuUnbuilder processedUnitUnbuilder
	UnitProc    UnitProcessor
}

func (rp CsvRowProcessor) Process(row []string) (outRow []string, rowError error, err error) {
	pu, err := rp.PuBuilder(row)
	if err != nil {
		return nil, nil, fmt.Errorf("when processing row '%v', %v", row, err)
	}
	res := rp.UnitProc.Process(pu)
	if err := res.Error(); err != nil {
		return nil, nil, fmt.Errorf("when processing row '%v', %v", row, err)
	}
	outRow, rowErr, err := rp.PuUnbuilder(res)
	if rowErr != nil {
		return outRow, fmt.Errorf("the processing on the row '%v' ended in error, %v", row, rowErr), nil
	}
	if err != nil {
		return nil, nil, fmt.Errorf("when processing row '%v', %v", row, err)
	}
	return outRow, nil, nil
}

type processedUnitBuilder = func([]string) (ProcessingUnit, error)
type processedUnitUnbuilder = func(ProcessingUnit) ([]string, error, error)
