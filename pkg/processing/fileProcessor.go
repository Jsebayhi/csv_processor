package processing

import "fmt"

type FileProcessor interface {
	Process(inputPath string, outputPath string) error
}

// Implementation

type CsvFileProcessor struct {
	ContentProc  ContentProcessor
	InputReader  csvReader
	OutputWriter csvWriter
	ErrorHandler ErrorHandler
}

func (fp CsvFileProcessor) Process(inputPath string, outputPath string) error {
	err := fp.ErrorHandler.Initialize()
	if err != nil {
		return fmt.Errorf("unable to setup logger for processing, %v", err)
	}
	defer fp.ErrorHandler.Close()
	fileContent, err := fp.InputReader.Read(inputPath)
	if err != nil {
		return fmt.Errorf("when reading file %s, %v", inputPath, err)
	}
	result, err := fp.ContentProc.Process(fileContent)
	if err != nil {
		return fmt.Errorf("when processing content from file %s, %v", inputPath, err)
	}
	if err := fp.OutputWriter.Write(outputPath, result); err != nil {
		return fmt.Errorf("when writing processing result to %s, %v", outputPath, err)
	}
	nonFatal := fp.ContentProc.NonFatalErrors()
	for _, err := range nonFatal {
		fp.ErrorHandler.Handle(err)
	}
	return nil
}
