package processing

var DefaultProcessingUnitFactory = NewDefaultProcessingUnitFactory()

func NewDefaultProcessingUnitFactory() ProcessingUnitFactory {
	return ProcessingUnitFactory{
		builder: map[ProcessingMode]ProcessingUnitBuilder{
			IterativeProcessingMode: NewIterativeProcessingUnit,
			ImmutableProcessingMode: NewImmutableProcessingUnit,
		},
	}
}

type ProcessingUnitFactory struct {
	builder map[ProcessingMode]ProcessingUnitBuilder
}

func (f ProcessingUnitFactory) Build(mode ProcessingMode, input Unit) ProcessingUnit {
	return f.builder[mode](input)
}

func (f ProcessingUnitFactory) Builder(mode ProcessingMode) ProcessingUnitBuilder {
	return f.builder[mode]
}
