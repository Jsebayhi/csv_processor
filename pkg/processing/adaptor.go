package processing

import "bitbucket.org/Jsebayhi/csv_processor/pkg/merrors"

type csvWriter = interface {
	Write(path string, records [][]string) error
}

type csvReader = interface {
	Read(path string) ([][]string, error)
}

type rule interface {
	Apply(ProcessingUnit) (ProcessingUnit, error)
	ValidateInputSchema(schemaValidator) error
	ValidateOutputSchema(schemaValidator) error
}

type schemaValidator = interface {
	ValidateFields([]fieldName) merrors.Errors
	ValidateField(fieldName) error
}

type schemaHeaderBuilder = interface {
	GetFields() []fieldName
}

type schemaDescriptor = interface {
	GetFields() []fieldName
	GetFieldPosition(fieldName) (int, error)
}

type fieldName = string
