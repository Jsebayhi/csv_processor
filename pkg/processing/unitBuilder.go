package processing

import (
	"fmt"
)

type UnitBuilder = interface {
	Build([]string) (Unit, error)
}

// Standard implementation

type StdUnitBuilder struct {
	Sch       schemaDescriptor
	BuildUnit func() Unit
}

func (b StdUnitBuilder) Build(row []string) (Unit, error) {
	u := b.BuildUnit()
	for _, f := range b.Sch.GetFields() {
		pos, err := b.Sch.GetFieldPosition(f)
		if err != nil {
			return u, fmt.Errorf("when building unit for given row %v, %v", row, err)
		}
		u.Set(f, row[pos])
	}
	return u, nil
}
