package processing

/*
import (
	"fmt"
)

type processingUnitBuilder = interface {
	Build([]string) (ProcessingUnit, error)
}

type processingUnitUnBuilder = interface {
	Unbuild(ProcessingUnit) ([]string, error)
}

// Standard implementation

type StdProcessedUnitBuilder struct {
	Sch                 schemaDescriptor
	BuildProcessingUnit ProcessingUnitBuilder
	BuildUnit           func() Unit
}

func (b StdProcessedUnitBuilder) Build(row []string) (pu ProcessingUnit, err error) {
	u := b.BuildUnit()
	for _, f := range b.Sch.GetFields() {
		pos, err := b.Sch.GetFieldPosition(f)
		if err != nil {
			return pu, fmt.Errorf("when building unit for given row %v, %v", row, err)
		}
		u.Set(f, row[pos])
	}
	return b.BuildProcessingUnit(u), nil
}

type StdProcessedUnitUnBuilder struct {
	Sch                      schemaDescriptor
	FailOnMissingOutputField bool
}

func (b StdProcessedUnitUnBuilder) UnBuild(pu ProcessingUnit) (row []string, err error) {
	row = make([]string, len(b.Sch.GetFields()))
	output := pu.GetOutput()
	for _, f := range b.Sch.GetFields() {
		pos, err := b.Sch.GetFieldPosition(f)
		if err != nil {
			return row, fmt.Errorf("when unbuilding unit for given processing unit %v, %v", pu, err)
		}
		if output.DoesContain(f) {
			outField, err := output.Get(f)
			if err != nil {
				return row, fmt.Errorf("when retrieving content of field %s, %v", f, err)
			}
			row[pos] = outField
		} else if b.FailOnMissingOutputField {
			return row, fmt.Errorf("field not found in the unit output: %s", f)
		} else {
			row[pos] = ""
		}
	}
	return row, nil
}
*/
