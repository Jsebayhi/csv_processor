package processing

type UnitProcessor = interface {
	Process(ProcessingUnit) ProcessingUnit
}

// Implmentations

type CsvUnitProcessor struct {
	R rule
}

//TODO: add context to error
func (up CsvUnitProcessor) Process(pu ProcessingUnit) ProcessingUnit {
	updatedPu, err := up.R.Apply(pu)
	if err != nil {
		updatedPu.RegisterError(err)
	}
	return updatedPu
}
