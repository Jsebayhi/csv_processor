package processing

import (
	"fmt"
)

type UnitUnBuilder = interface {
	Unbuild(Unit) ([]string, error)
}

// Standard implementation

type StdUnitUnBuilder struct {
	Sch                      schemaDescriptor
	FailOnMissingOutputField bool
	DefaultRowContent        string
}

func (b StdUnitUnBuilder) Unbuild(unit Unit) ([]string, error) {
	row := make([]string, len(b.Sch.GetFields()))
	for _, f := range b.Sch.GetFields() {
		pos, err := b.Sch.GetFieldPosition(f)
		if err != nil {
			return row, fmt.Errorf("when unbuilding unit %v, %v", unit, err)
		}
		if unit.DoesContain(f) {
			outField, err := unit.Get(f)
			if err != nil {
				return row, fmt.Errorf("when retrieving content of field %s, %v", f, err)
			}
			row[pos] = outField
		} else if b.FailOnMissingOutputField {
			return row, fmt.Errorf("field not found in the unit output: %s", f)
		} else {
			row[pos] = b.DefaultRowContent
		}
	}
	return row, nil
}
