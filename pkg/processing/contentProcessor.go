package processing

import (
	"errors"
	"fmt"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/merrors"
)

type ContentProcessor interface {
	// Return the resulting content minus the line where a non fatal error
	// occured or the first fatal error which occured
	Process([][]string) (res [][]string, fatal error)

	// Return errors which occured but didn't compromise further computation
	// during the last process.
	NonFatalErrors() []error
}

// Implementation

// Content processor which handle first line as header.
type CsvContentProcessor struct {
	HeaderProcessor RowProcessor
	BodyProcessor   ContentProcessor
	nonFatalErrors  merrors.ErrorsBuilder
}

func (ccp CsvContentProcessor) NonFatalErrors() []error {
	if ccp.nonFatalErrors == nil {
		return []error{errors.New("retriving error on unstarted processor")}
	}
	return ccp.nonFatalErrors.Enrich()
}

func (ccp CsvContentProcessor) Process(content [][]string) (res [][]string, fatal error) {
	newHeader, rowErr, err := ccp.HeaderProcessor.Process(content[0])
	if err != nil {
		return nil, fmt.Errorf("a fatal error occured during content processing, %v", err)
	} else if err != nil {
		return nil, fmt.Errorf("a fatal error occured during content processing, %v", rowErr)
	}
	newBody, err := ccp.BodyProcessor.Process(content[1:])
	if err != nil {
		return nil, fmt.Errorf("a fatal error occured during content processing, %v", err)
	}

	ccp.nonFatalErrors = merrors.NewErrorsBuilder("during content processing, ")
	ccp.nonFatalErrors.Append(ccp.BodyProcessor.NonFatalErrors()...)

	return append([][]string{newHeader}, newBody...), nil
}

// Content processor which handle content as header free.
type CsvBodyProcessor struct {
	RowProc        RowProcessor
	nonFatalErrors merrors.ErrorsBuilder
}

func (bp CsvBodyProcessor) NonFatalErrors() []error {
	if bp.nonFatalErrors == nil {
		return []error{errors.New("retriving error on unstarted processor")}
	}
	return bp.nonFatalErrors.Enrich()
}

func (bp CsvBodyProcessor) Process(b [][]string) (res [][]string, fatal error) {
	bp.nonFatalErrors = merrors.NewErrorsBuilder("when processing body")
	for _, row := range b {
		r, rowErr, err := bp.RowProc.Process(row)
		if err != nil {
			return res, fmt.Errorf("when processing body, %v", err)
		} else if rowErr != nil {
			bp.nonFatalErrors.Append(rowErr)
		} else {
			res = append(res, r)
		}
	}
	return
}
