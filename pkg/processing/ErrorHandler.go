package processing

import (
	"fmt"
	"os"
)

type ErrorHandler interface {
	Initialize() error
	Handle(error) error
	Close() error
}

type FileLogger struct {
	Path string
	f    *os.File
}

func (eh *FileLogger) Initialize() (err error) {
	eh.f, err = os.OpenFile(eh.Path, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return fmt.Errorf("file logger initialization failed, %v", err)
	}
	return
}

func (eh FileLogger) Handle(e error) (err error) {
	if eh.f == nil {
		return fmt.Errorf("file logger is not initialized")
	}
	_, err = eh.f.WriteString(e.Error())
	if err != nil {
		return fmt.Errorf("unable to log an error, is error nil: %v, error when logging: %v", e == nil, err)
	}
	return
}

func (eh *FileLogger) Close() error {
	err := eh.f.Close()
	if err != nil {
		return fmt.Errorf("unable to close the file to which the logger append, %v", err)
	}
	eh.f = nil
	return nil
}
