package processing

import (
	"bitbucket.org/Jsebayhi/csv_processor/pkg/merrors"
)

type ProcessingUnit = interface {
	GetInput() Unit
	GetOutput() Unit
	SetOutput(Unit) error
	RegisterError(error)
	Error() error
}

type ProcessingMode string

func (m ProcessingMode) String() string {
	return string(m)
}

type ProcessingUnitBuilder = func(Unit) ProcessingUnit

// Implementations

const ImmutableProcessingMode ProcessingMode = "ImmutableProcessingMode"

// Build a processing unit which considered the initial input as immutable.
func NewImmutableProcessingUnit(input Unit) ProcessingUnit {
	return &immutableProcessingUnit{
		in:     input,
		out:    input,
		errors: merrors.NewErrorsBuilder("during immutable processing"),
	}
}

// The immutable processing unit always give the original input when asked for it.
type immutableProcessingUnit struct {
	in     Unit
	out    Unit
	errors merrors.ErrorsBuilder
}

func (pu immutableProcessingUnit) GetInput() Unit {
	return pu.in
}

func (pu immutableProcessingUnit) GetOutput() Unit {
	return pu.out
}

func (pu *immutableProcessingUnit) SetOutput(o Unit) error {
	pu.out = o
	return nil
}

func (pu *immutableProcessingUnit) RegisterError(err error) {
	pu.errors.Append(err)
}

func (pu immutableProcessingUnit) Error() error {
	return pu.errors.Build()
}

const IterativeProcessingMode ProcessingMode = "IterativeProcessingMode"

// Build a processing unit which gives back it's last registered output as input.
func NewIterativeProcessingUnit(in Unit) ProcessingUnit {
	return &iterativeProcessingUnit{
		state:  in,
		errors: merrors.NewErrorsBuilder("during iterative processing"),
	}
}

// The iterative processing unit give back the last registered output as input.
type iterativeProcessingUnit struct {
	state  Unit
	errors merrors.ErrorsBuilder
}

func (pu iterativeProcessingUnit) GetInput() Unit {
	return pu.state
}

func (pu iterativeProcessingUnit) GetOutput() Unit {
	return pu.state
}

func (pu *iterativeProcessingUnit) SetOutput(o Unit) error {
	pu.state = o
	return nil
}

func (pu *iterativeProcessingUnit) RegisterError(err error) {
	pu.errors.Append(err)
}

func (pu iterativeProcessingUnit) Error() error {
	return pu.errors.Build()
}
