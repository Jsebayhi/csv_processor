package yaml

import (
	"io/ioutil"

	"gopkg.in/yaml.v2"
)

// IO
func LoadYamlFile(path string, out interface{}) error {
	fileContent, err := ioutil.ReadFile(path)
	if err != nil {
		return err
	}

	err = yaml.Unmarshal(fileContent, out)
	if err != nil {
		return err
	}
	return nil
}
