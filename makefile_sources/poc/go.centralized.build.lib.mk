###########
# LIBRARY #
###########

# TODO
######
# TODO: use auto discovery on one level in the cmd folder when CMDS_NAMES is not defined

# DOC
#####
#
# overview
#---------
#
# Allow centralized management of cmd build.
#
# required folder structre:
#
# - ${BIN_PATH} -- where built executable are stored
# - ${CMD_PATH} -- go package containing main function
#
# ├── ${BIN_PATH}:
# │   ├── exe0
# │   ├── ...
# │   ├── exen
# ├── ${CMD_PATH}:
# │   ├── exe0
# │   |   ├── exe0.go
# │   |   ├── ...
# │   |   ├── exe0n.go
# │   ├── ...
# │   ├── exeN
# │   |   ├── exeN.go
# │   |   ├── ...
# │   |   ├── exeNn.go
#
# technical
#----------
#
# 'bd_' is used as a prefix for every rules of this MK in order to avoid 
# collision, overriding or inheritance and delegate rule composition to the MAIN
# Makefile.
# Exceptions are made on some rules which are being given a folder name in order
# to avoid it to be replayed if it's result (the file name designated by the 
# rule) are already apparent, others have been added to the '.PHONY'

# REQUIRED
##########
# - MAKEFILE_SOURCES
# - BIN_PATH
# - CMD_PATH
# - CMDS_NAMES

# Optional
##########

##########
# IMPORT #
##########
include $(MAKEFILE_SOURCES)/ft.lib.mk

# Const
#######

# Dynamic rule names
#-------------------

BINS=$(addprefix $(BIN_PATH:./%=%)/, $(CMDS_NAMES))
BD_BINS=$(BINS:%=bd_%)
BINS_CLEAN=$(BD_BINS:%=%_clean)

#########
# RULES #
#########

# Dynamic help
#-------------

.PHONY: bd_show_dynamic_rules
bd_show_dynamic_rules: ## show available cmd specific build rules
	@echo "cmd build: "$(BINS)
	@echo "cmd clean: "$(BINS_CLEAN)
	

# Build
#------

.PHONY: bd_build_all
bd_build_all: $(BINS) ## build all commands

# Use own go.mod file of the cmd to build.
$(BINS): ## /!\ not printed by help section
	mkdir -p $(BIN_PATH)
	cd $(CMD_PATH)/$(notdir $(@:bd_%=%)); go build -a -race -o $(call ft_backwardPath,$(CMD_PATH))/$(@:bd_%=%)

# Clean
#------

.PHONY: bd_clean
bd_clean: ## delete the bin directory
	rm -rf $(BIN_PATH)

.PHONY: $(BINS_CLEAN)
$(BINS_CLEAN): ## /!\ not printed by help section
	rm -f $(@:bd_%_clean=%)

# Rebuild
#--------

.PHONY: bd_re_all
bd_re_all: bd_clean bd_build_all ## clean then rebuild the program