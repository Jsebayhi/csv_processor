###########
# LIBRARY #
###########

# TODO
######
# TODO: use auto discovery on one level in the MODULES_PATH folder when MODULES_NAMES is not defined
# add handling of release

# DOC
#####
#
# overview
#---------
#
# Allow centralized dependency management for go sources
#
# required folder structre:
#
# ├── ${MODULES_PATH}:
# │   ├── mod0
# │   |   ├── v0
# │   |   |   ├── go.mod
# │   |   |   ├── go.sum
# │   |   |   ├── mod0.go
# │   |   |   ├── ...
# │   |   |   ├── modMn.go
# │   |   ├── ...
# │   |   ├── vM
# │   |   |   ├── go.mod
# │   |   |   ├── go.sum
# │   |   |   ├── mod0.go
# │   |   |   ├── ...
# │   |   |   ├── modMn.go
# │   ├── ...
# │   ├── modN
# │   |   ├── v0
# │   |   |   ├── go.mod
# │   |   |   ├── go.sum
# │   |   |   ├── mod0.go
# │   |   |   ├── ...
# │   |   |   ├── modMn.go
# │   |   ├── ...
# │   |   ├── vM
# │   |   |   ├── go.mod
# │   |   |   ├── go.sum
# │   |   |   ├── mod0.go
# │   |   |   ├── ...
# │   |   |   ├── modMn.go
#
# technical
#----------
#
# 'dep_' is used as a prefix for every rules of this MK in order to avoid 
# collision, overriding or inheritance and delegate rule composition to the MAIN
# Makefile.
# Exceptions are made on some rules which are being given a folder name in order
# to avoid it to be replayed if it's result (the file name designated by the 
# rule) are already apparent, others have been added to the '.PHONY'

# REQUIRED
##########
# - MAKEFILE_SOURCES
# - PKG
# - MODULES_PATH
# - MODULES_NAMES

# Optional
##########

##########
# IMPORT #
##########
include $(MAKEFILE_SOURCES)/ft.lib.mk

# Const
#######
MODULES=$(addprefix $($(MODULES_PATH:.%=%):/%=%), $(MODULES_NAMES))
DEP_MODULES=$(MODULES:%=dep_%)

# Dynamic rule names
#-------------------

# alias name for MODULES_GO_MOD
MODULES_INIT=$(addsuffix _init, $(DEP_MODULES))
MODULES_GO_MOD=$(addsuffix /go.mod, $(MODULES))
MODULES_BUILD=$(addsuffix _build, $(DEP_MODULES))
MODULES_UPDATE_TO_LATEST_MINOR=$(addsuffix _update_to_latest_minor, $(DEP_MODULES))
MODULES_CLEAN=$(addsuffix _clean, $(DEP_MODULES))
MODULES_FCLEAN=$(addsuffix _fclean, $(DEP_MODULES))
MODULES_GIT_PUBLISH=$(addsuffix _git_publish, $(DEP_MODULES))
MODULES_GIT_TAG=$(addsuffix _git_tag, $(DEP_MODULES))

#########
# RULES #
#########

.PHONY: dep_all
dep_all: dep_init dep_update_to_latest_minor dep_build_all dep_clean

# Dynamic help
#-------------

.PHONY: dep_show_dynamic_rules
dep_show_dynamic_rules: ## show dynamic rules help
	@echo "module init:                   "$(MODULES_INIT)
	@echo "module build:                  "$(MODULES_BUILD)
	@echo "module update to latest minor: "$(MODULES_UPDATE_TO_LATEST_MINOR)
	@echo "module clean:                  "$(MODULES_CLEAN)
	@echo "module fclean:                 "$(MODULES_FCLEAN)
	@echo "module publish to git:         "$(MODULES_GIT_PUBLISH)

# Init module
#------------

.PHONY: dep_init
dep_init: $(MODULES_GO_MOD) ## init modules if not already done

# user friendly alias for MODULES_GO_MOD rules
.PHONY: $(MODULES_INIT)
$(MODULES_INIT): $(@:%_init=%/go.mod)## /!\ not printed by help section - init the module

$(MODULES_GO_MOD): ## /!\ not printed by help section - init the module if not already done
	cd $(@:%/go.mod=%) && go mod init $(PKG)/$(@:%/go.mod=%)

# Building go module registered dependencies
#-------------------------------------------

.PHONY: dep_build_all
dep_build_all: $(MODULES_BUILD) ## update modules registered dependencies

.PHONY: $(MODULES_BUILD)
$(MODULES_BUILD): ## /!\ not printed by help section - build the module
	cd $(@:dep_%_build=%) &&\
	go build -a -race ./... &&\
	go test -race ./...

# Cleaning / pruning
#-------------------

.PHONY: dep_clean
dep_clean: $(MODULES_CLEAN) ## clean / prune all module dependencies

.PHONY: $(MODULES_CLEAN)
$(MODULES_CLEAN): ## /!\ not printed by help section - remove no longer needed dependencies
	cd $(@:dep_%_clean=%); go mod tidy

# Update management
#------------------

.PHONY: dep_show_avail_updates
dep_show_avail_updates: ## show available dependencies updates
	go list -u -m all

.PHONY: dep_update_to_latest_minor
dep_update_to_latest_minor: $(MODULES_UPDATE_TO_LATEST_MINOR)

.PHONY: $(MODULES_UPDATE_TO_LATEST_MINOR)
$(MODULES_UPDATE_TO_LATEST_MINOR): ## update dependencies' version to latest minors or patch
	cd $(@:dep_%_update_to_latest_minor=%); go get -u

# Fclean
#-------

.PHONY: dep_fclean
dep_fclean: $(MODULES_FCLEAN) ## remove every modules's specification

.PHONY: $(MODULES_FCLEAN)
$(MODULES_FCLEAN): ## /!\ not printed by help section - remove the module's specification
	cd $(@:dep_%_fclean=%) &&\
	rm -f go.mod go.sum

# Publish
#--------

.Phony: dep_git_publish
dep_git_publish: $(MODULES_GIT_PUBLISH) ## add and commit to git every modules' dependency changes by doing one commit per module

.PHONY: $(MODULES_GIT_PUBLISH)
$(MODULES_GIT_PUBLISH): ## /!\ not printed by help section - add and commit the module dependency change
	git reset &&\
	cd $(@:dep_%_git_publish=%) &&\
	git add go.mod go.sum &&\
	git commit -m "[$(@:dep_%_git_publish=%)] update dependency" ||\
	echo "nothing to do"

# Release 
#--------

release_major:
	git branch

.PHONY: dep_git_tag
dep_git_tag: $(MODULES_GIT_TAG) ## tag release for every module

.PHONY: $(MODULES_GIT_TAG)
$(MODULES_GIT_TAG):
	cd $(@:dep_%_git_tag=%) && pwd &&\
	VERSION=$$(cat go.mod | grep module | cut -d' ' -f2 |\
	rev | cut -d '/' -f1 | rev | grep -E "^v[0-9]+(\.[0-9]){0,2}$$");\
	echo git tag $(@:dep_%_git_tag=%)/$${VERSION:=v1}