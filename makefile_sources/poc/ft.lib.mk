###########
# LIBRARY #
###########

# DOC
#####
#
# Functionality library

# REQUIRED
##########

# Optional
##########

# Const
#######
noop=
space = $(noop) $(noop)

##########
# IMPORT #
##########

####################
# FUNCTIONNALITIES #
####################

# backwardPath
# change dir/dir2/dir3
# into   ../../..
ft_backwardPath = $(subst $(space),/,$(patsubst %,.., $(subst /,$(space), $1)))

#########
# RULES #
#########