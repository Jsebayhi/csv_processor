###########
# LIBRARY #
###########

# DOC
#####

# REQUIRED
##########

# Optional
##########

# Const
#######
SHELL = /bin/bash

##########
# IMPORT #
##########

#########
# RULES #
#########

.PHONY: todo
todo: ## list all todos (TPATH="the/path", EXT=".ext" to refine search)
	@echo "in" $(shell basename $(shell pwd))"/${TPATH}"
	@for f in $$(find ./${TPATH} -type f -name "*${EXT}" | grep -v ".git") ; do\
		TODOS=$$(cat ./$$f | grep "TODO" | grep -v "TODOS") ;\
		todos=$$(cat ./$$f | grep "todo" | grep -v "todos") ;\
		if [ -n "$${TODOS}" ] || [ -n "$${todos}" ];then \
			echo "in" $$f ":" ; \
			if [ -n "$${TODOS}" ];then cat ./$$f | grep "TODO" | grep -v "TODOS";fi; \
			if [ -n "$${todos}" ];then cat ./$$f | grep "todo" | grep -v "todos";fi; \
		fi;\
	done

.PHONY: show_rules
# rules' name with length over 25 are truncated.
# credit: https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
show_rules: ## show available rules
	@grep -h -E '^[0-9a-zA-Z_-]+:' $(MAKEFILE_LIST) | grep -v "### not shown" | cut -d':' -f1 | awk '{printf "\033[36m%-25s\033[0m \n", $$1}'

.PHONY: show_described_rules
# rules' name with length over 25 are truncated.
# credit: https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
show_described_rules: ## show available described rules
	@grep -h -E '^[0-9a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | grep -v "### not shown" | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'

.PHONY: help
# rules' name with length over 25 are truncated.
# credit: https://marmelab.com/blog/2016/02/29/auto-documented-makefile.html
help: ## show available rules with their description if exist
	@grep -h -E '^[0-9a-zA-Z_-]+:' $(MAKEFILE_LIST) | grep -v "### not shown" | awk 'BEGIN {FS = ":(.*?## )?"}; {printf "\033[36m%-25s\033[0m %s\n", $$1, $$2}'