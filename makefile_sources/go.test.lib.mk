###########
# LIBRARY #
###########

# DOC
#####

# REQUIRED
##########

# Optional
##########

# Const
#######

##########
# IMPORT #
##########

#########
# RULES #
#########

.PHONY: example
example: ## build examples (can specify a path by doing TPATH="the/path")
	go build -tags=example ./${TPATH}/...

.PHONY: unit-test
unit-test: ## run unit tests with race detection for current path or TPATH="the/path"
	go test -v -race -tags=unit ./${TPATH}/... | grep -v "=== RUN"

.PHONY: integration-test
integration-test: ## run integration tests with race detection for current path or TPATH="the/path"
	go test -v -race -tags=integration ./${TPATH}/... | grep -v "=== RUN"
