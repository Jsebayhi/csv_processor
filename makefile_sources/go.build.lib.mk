###########
# LIBRARY #
###########

# DOC
#####

# REQUIRED
##########
# BIN_PATH
# BIN_NAME

# Optional
##########

# Const
#######

##########
# IMPORT #
##########

####################
# FUNCTIONNALITIES #
####################

#########
# RULES #
#########

.PHONY: build
build: $(BIN_PATH)/$(BIN_NAME) ## build the program
build = defined # For compatibility with mod mk

$(BIN_PATH)/$(BIN_NAME):
	mkdir -p $(BIN_PATH)
	go build -a -race -o $(BIN_PATH)/$(BIN_NAME)

.PHONY: run
run: ## run the program
	$(BIN_PATH)/$(BIN_NAME)

.PHONY: clean
clean: ## delete the program executable
	rm -f $(BIN_PATH)/$(BIN_NAME)
clean = defined # For compatibility with mod mk

.PHONY: re
re: clean build ## clean then rebuild the program