########
# BASE #
########

# DOC
#####

# REQUIRED
##########
# - MAKEFILE_SOURCES
# - PKG
# - BIN_PATH
# - BIN_NAME

# Optional
##########

# Const
#######

##########
# IMPORT #
##########
#include $(MAKEFILE_SOURCES)/src.lib.mk

####################
# FUNCTIONNALITIES #
####################

#########
# RULES #
#########

include $(MAKEFILE_SOURCES)/common.lib.mk
include $(MAKEFILE_SOURCES)/go.test.lib.mk
include $(MAKEFILE_SOURCES)/go.build.lib.mk
include $(MAKEFILE_SOURCES)/go.module.lib.mk