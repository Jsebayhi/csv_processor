########
# BASE #
########

# DOC
#####

# REQUIRED
##########
# - MAKEFILE_SOURCES

# Optional
##########

# Const
#######

##########
# IMPORT #
##########
#include $(MAKEFILE_SOURCES)/src.lib.mk

####################
# FUNCTIONNALITIES #
####################

#########
# RULES #
#########

include $(MAKEFILE_SOURCES)/common.lib.mk
include $(MAKEFILE_SOURCES)/go.test.lib.mk
include $(MAKEFILE_SOURCES)/go.module.lib.mk