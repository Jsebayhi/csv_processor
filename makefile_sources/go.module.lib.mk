###########
# LIBRARY #
###########

# DOC
#####
#
# Allow simple module management for go sources
#
# Folder structre:
#-----------------
#
# ├── ${MODULE_NAME}:
# │   ├── go.mod
# │   ├── go.sum
# │   ├── files.go
#
# technical
#----------
#
# Use https://github.com/marwan-at-work/mod for version bumping before release.
#
# ressources
#-----------
#
# About go modules:
# https://github.com/golang/go/wiki/Modules
#
# go get private repo:
# https://stackoverflow.com/a/52089159/4912384


# REQUIRED
##########
# -PKG

# Optional
##########
BIN_PATH?=./
MODULE_NAME?=$(notdir $(shell pwd))

# Const
#######

##########
# IMPORT #
##########

#########
# RULES #
#########

# Init
#-----

.PHONY: init
mod_init: go.mod ## initialise the module if not already done

go.mod:
	go mod init ${PKG}

# build
#------

ifndef build # For compatibility with cmd mk
.PHONY: build
build: ### not shown
	go build -a -race ./...
endif

ifndef clean # For compatibility with cmd mk
.PHONY: clean
clean: ### not shown
	@echo -n
endif

.PHONY: mod_build
mod_build: clean build ## build and prune dependencies
	go test all
	go mod tidy

.PHONY: mod_light_build
mod_light_build: clean build ## build and prune direct dependencies
	go test ./...
	go mod tidy

# dependency
#-----------

.PHONY: mod_update_to_latest_snapshot
mod_update_to_latest_snapshot: ## update dependency' version to latest available, snapshot or else (TODO)
	cat go.mod | grep require | cut -d' ' -f2
	go get -u @latest

.PHONY: mod_update_to_latest_patch
mod_update_to_latest_patch: ## update dependency' version to latest patch
	go get -u=patch

.PHONY: mod_update_to_latest_minor
mod_update_to_latest_minor: ## update dependency' version to latest minors or patch
	go get -u

# Release
#--------

.PHONY: mod_release_major
mod_release_major: ## increase major, git commit then tag (TODO)

.PHONY: mod_release_minor
mod_release_minor: ## increase minor, git commit then tag (TODO)

.PHONY: mod_release_patch
mod_release_patch: ## increase patch, git commit then tag (TODO)

.PHONY: mod_bump
mod_bump: ## Increase version TARGET by 1 (TODO)

.PHONY: mod_release
mod_release:
	VERSION=$$(cat go.mod | grep module | cut -d' ' -f2 |\
	rev | cut -d '/' -f1 | rev | grep -E "^v[0-9]+(\.[0-9]){0,2}$$") \
	|| VERSION=v1 \
	&& git reset \
	&& git add go.mod go.sum \
	&& git commit -m "[$(@:dep_%_git_publish=%)] update dependency" \
	&& echo git tag $(MODULE_NAME)/$${VERSION} \
	|| echo failed to commit and tag

# Publish
#--------

.PHONY: mod_publish
mod_publish: ## git push
	git push
