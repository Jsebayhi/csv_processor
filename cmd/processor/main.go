package main

import (
	"fmt"
	"log"
	"time"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/csv"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/data"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/process"
)

type csvContent = [][]string

func init() {
}

func main() {
	args := extractProgramArgs(Args{ConfigurationPath: "conf.yaml"})
	conf := extractConfiguration(args.ConfigurationPath)

	outputPath := FormatOutputFilePath(conf, time.Now())
	log.Println("output path: " + outputPath)

	err := Process(conf, conf.Input.Path, outputPath)
	if nil != err {
		log.Println(err)
	} else {
		log.Println("Finished")
	}
}

// IO
func extractConfiguration(path string) Config {
	conf, err := LoadConfiguration(path)
	if err != nil {
		log.Panicf("when loading the configuration at the path %s: %v", path, err)
	}
	log.Println("Configuration loaded from path: ", path)
	log.Println("Configuration content: ", conf)
	return conf
}

//IO
func Process(conf Config, csvPath string, outputPath string) error {
	inputCsvContent, err := csv.LoadCsv(csvPath, conf.Input.Schema)
	if err != nil {
		return fmt.Errorf("when loading the csv at the path %s: %v", csvPath, err)
	}

	inputHeaderValidator := data.NewInputHeaderValidator(conf.Input.Schema)
	inputValidator := data.NewInputValidator(conf.Input.Schema)
	inputFormatter := data.NewInputFormatter(conf.Input.Schema)
	outputValidator := data.NewOutputValidator(conf.Output.Schema)
	outputFormatter := data.NewOutputFormatter(conf.Output.Schema)
	outputHeaderFormatter := data.NewOutputHeaderFormatter(conf.Output.Schema)

	inputProcessor, err := process.NewCsvLineProcessor(conf.Rule, conf.Input.Schema, conf.Output.Schema, process.IterativeMode)
	if nil != err {
		return fmt.Errorf("when instanciating new csv line processor")
	}

	orchestrator := process.NewOrchestrator(
		inputHeaderValidator,
		inputValidator,
		inputFormatter,
		inputProcessor,
		outputValidator,
		outputFormatter,
		outputHeaderFormatter,
	)
	err = orchestrator.ValidateInputSchema(conf.Input.Schema)
	if nil != err {
		return fmt.Errorf("when validating pipeline input schema")
	}
	err = orchestrator.ValidateOutputSchema(conf.Input.Schema)
	if nil != err {
		return fmt.Errorf("when validating pipeline output schema")
	}

	processedCsvContent, err := orchestrator.ProcessAll(inputCsvContent)
	if nil != err {
		return fmt.Errorf("when processing csvContent: %v", err)
	}

	//Save csv
	if err := csv.SaveCsv(outputPath, processedCsvContent, conf.Output.Schema); err != nil {
		return fmt.Errorf("when saving the processed csv file at the path %s: %v", outputPath, err)
	}
	return nil
}
