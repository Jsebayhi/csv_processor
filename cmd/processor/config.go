package main

import (
	"fmt"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/rule"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/schema"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/yaml"
)

func LoadConfiguration(path string) (config Config, err error) {
	rawConfig := RawConfig{}
	if err = yaml.LoadYamlFile(path, &rawConfig); err != nil {
		return config, fmt.Errorf("unable to load the yaml configuration: %v", err)
	}
	if config, err = FromRawConfig(rawConfig); err != nil {
		return config, fmt.Errorf("unable to convert the raw configuration to a configuration: %v", err)
	}
	return config, nil
}

// Config
//

type Config struct {
	Name   string
	Input  Input
	Output Output
	Rule   rule.Rule
}

func FromRawConfig(rawConfig RawConfig) (config Config, err error) {
	config.Name = rawConfig.NormalizerName
	if config.Input, err = FromRawInputConfig(rawConfig.Input); err != nil {
		return config, err
	}
	if config.Output, err = FromRawOutputConfig(rawConfig.Output); err != nil {
		return config, err
	}
	config.Rule = rule.Identity{}
	return config, nil
}

// INPUT
//

type Input struct {
	Path   string
	Schema schema.CsvSchema
}

func FromRawInputConfig(inputConf RawInputConfig) (input Input, err error) {
	input.Path = inputConf.Path
	if input.Schema, err = schema.LoadCsvSchemaFromYamlFile(inputConf.SchemaPath); err != nil {
		return input, err
	}
	return input, nil
}

// OUTPUT
//

type Output struct {
	Path   string
	Schema schema.CsvSchema
}

func FromRawOutputConfig(outputConf RawOutputConfig) (output Output, err error) {
	output.Path = outputConf.Path
	if output.Schema, err = schema.LoadCsvSchemaFromYamlFile(outputConf.SchemaPath); err != nil {
		return output, err
	}
	return output, nil
}
