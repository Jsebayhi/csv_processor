package main

import "flag"

type Args struct {
	ConfigurationPath string
}

// IO
func extractProgramArgs(defaultArgs Args) Args {
	var configArgs Args

	flag.StringVar(&configArgs.ConfigurationPath, "configurationPath", defaultArgs.ConfigurationPath, "path_to_file/fileName.yaml")
	flag.Parse()

	return configArgs
}
