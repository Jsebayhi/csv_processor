package main

import (
	"time"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/lib"
)

func FormatOutputFilePath(config Config, datetime time.Time) string {
	return config.Output.Path + "/" +
		config.Name + "-normalized-" +
		datetime.UTC().Format(lib.Iso8601TimeFormatter) + ".csv"
}
