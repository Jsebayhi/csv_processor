package main

//TODO: make package private
type RawConfig struct {
	NormalizerName string          `yaml:"name"`
	Input          RawInputConfig  `yaml:"input"`
	Output         RawOutputConfig `yaml:"output"`
}

//TODO: make package private
type RawInputConfig struct {
	Path       string `yaml:"path"`
	SchemaPath string `yaml:"schema_path"`
}

//TODO: make package private
type RawOutputConfig struct {
	Path       string `yaml:"path"`
	SchemaPath string `yaml:"schema_path"`
}
