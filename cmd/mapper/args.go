package main

import "flag"

type args struct {
	ConfigurationPath string
}

// IO
func extractProgramArgs(defaultArgs args) args {
	var configArgs args

	flag.StringVar(&configArgs.ConfigurationPath, "configurationPath", defaultArgs.ConfigurationPath, "path_to_file/fileName.yaml")
	flag.Parse()

	return configArgs
}
