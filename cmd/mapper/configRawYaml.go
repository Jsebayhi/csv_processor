package main

import (
	"fmt"
	"strings"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/action"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/csv"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/lib"
)

type rawConfig struct {
	Name    string          `yaml:"name"`
	Input   rawInputConfig  `yaml:"input"`
	Output  rawOutputConfig `yaml:"output"`
	Mapping RawMapping      `yaml:"mapping"`
	Log     rawLogsConfig   `yaml:"logs"`
}

type rawInputConfig struct {
	Path       string `yaml:"path"`
	SchemaPath string `yaml:"schema_path"`
}

type rawOutputConfig struct {
	Path       string `yaml:"path"`
	SchemaPath string `yaml:"schema_path"`
}

type rawLogsConfig struct {
	RowProcessingError rawRowProcessingErrorConfig `yaml:"row_processing_errors"`
}

type rawRowProcessingErrorConfig struct {
	Path   string `yaml:"path"`
	Prefix string `yaml:"prefix"`
	Suffix string `yaml:"suffix"`
}

type RawMapping struct {
	Separator     string `yaml:"separator"`
	MappingPath   string `yaml:"mapping_path"`
	SrcColumnName string `yaml:"src_column_path"`
	DstColumnName string `yaml:"dst_column_path"`
}

func FromRawMappingConfig(mappingConf RawMapping) (mapping action.OneToOneMapping, err error) {
	csvSeparator, err := lib.StringToRune(mappingConf.Separator)
	if err != nil {
		return mapping, fmt.Errorf("invalid mapping file separator, %v", err)
	}

	reader := csv.NewReader(csv.FileSettings{Separator: csvSeparator})

	mappingFileContent, err := reader.Read(mappingConf.MappingPath)
	if err != nil {
		return mapping, fmt.Errorf("unable to load mappings, %v", err)
	}

	mapping = action.OneToOneMapping{Mapping: map[string]string{}}
	for _, ctt := range mappingFileContent {
		if len(ctt) != 2 {
			return mapping, fmt.Errorf("bad mapping file, a reference map to nothing: %v", ctt)
		}

		mappingRef := strings.TrimSpace(ctt[0])
		if mappingRef == "" {
			return mapping, fmt.Errorf("bad mapping file, a reference is empty: %v", ctt)
		}

		mappingResult := strings.TrimSpace(ctt[1])
		if mappingResult == "" {
			return mapping, fmt.Errorf("bad mapping file, a mapping result is empty: %v", ctt)
		}

		_, ok := mapping.Mapping[mappingRef]
		if ok {
			return mapping, fmt.Errorf("bad mapping file, a reference is present multiple times: %v", ctt[0])
		} else {
			mapping.Mapping[mappingRef] = mappingResult
		}
	}

	if strings.TrimSpace(mappingConf.DstColumnName) == "" {
		return mapping, fmt.Errorf("bad mapping conf, the destination column name is empty: %s", mappingConf.DstColumnName)
	}
	mapping.DstColumnName = mappingConf.DstColumnName

	if strings.TrimSpace(mappingConf.SrcColumnName) == "" {
		return mapping, fmt.Errorf("bad mapping conf, the destination column name is empty: %s", mappingConf.SrcColumnName)
	}
	mapping.SrcColumnName = mappingConf.SrcColumnName

	return mapping, nil
}
