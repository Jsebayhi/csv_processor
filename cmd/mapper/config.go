package main

import (
	"fmt"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/action"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/schema"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/yaml"
)

func LoadConfiguration(path string) (config config, err error) {
	rawConfig := rawConfig{}
	if err = yaml.LoadYamlFile(path, &rawConfig); err != nil {
		return config, fmt.Errorf("unable to load the yaml configuration: %v", err)
	}
	if config, err = FromRawConfig(rawConfig); err != nil {
		return config, fmt.Errorf("unable to convert the raw configuration to a configuration: %v", err)
	}
	return config, nil
}

// config
//

type config struct {
	Name    string
	Input   input
	Output  output
	Mapping action.OneToOneMapping
	Logs    logs
}

func FromRawConfig(rawConfig rawConfig) (config config, err error) {
	config.Name = rawConfig.Name
	if config.Input, err = FromRawInputConfig(rawConfig.Input); nil != err {
		return config, fmt.Errorf("bad config, %v", err)
	}
	if config.Output, err = FromRawOutputConfig(rawConfig.Output); nil != err {
		return config, fmt.Errorf("bad config, %v", err)
	}
	if config.Mapping, err = FromRawMappingConfig(rawConfig.Mapping); nil != err {
		return config, fmt.Errorf("bad config, %v", err)
	}
	config.Logs = FromRawLogsConfig(rawConfig.Log)
	return config, nil
}

// input
//

type input struct {
	Schema schema.CsvSchema
	Path   string
}

func FromRawInputConfig(inputConf rawInputConfig) (in input, err error) {
	in.Path = inputConf.Path
	if in.Schema, err = schema.LoadCsvSchemaFromYamlFile(inputConf.SchemaPath); err != nil {
		return in, err
	}
	return in, nil
}

// output
//

type output struct {
	Schema schema.CsvSchema
	Path   string
}

func FromRawOutputConfig(outputConf rawOutputConfig) (out output, err error) {
	out.Path = outputConf.Path
	if out.Schema, err = schema.LoadCsvSchemaFromYamlFile(outputConf.SchemaPath); err != nil {
		return out, err
	}
	return out, nil
}

// log
//

type logs struct {
	RowProcessingError rowProcessingError
}

func FromRawLogsConfig(raw rawLogsConfig) logs {
	return logs{
		RowProcessingError: FromRawRowProcessingErrorConfig(raw.RowProcessingError),
	}
}

type rowProcessingError struct {
	Path   string
	Prefix string
	Suffix string
}

func FromRawRowProcessingErrorConfig(raw rawRowProcessingErrorConfig) rowProcessingError {
	return rowProcessingError{
		Path:   raw.Path,
		Prefix: raw.Prefix,
		Suffix: raw.Suffix,
	}
}
