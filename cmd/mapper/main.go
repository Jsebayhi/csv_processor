package main

import (
	"log"

	"bitbucket.org/Jsebayhi/csv_processor/pkg/csv"
	"bitbucket.org/Jsebayhi/csv_processor/pkg/processing"
)

func init() {
}

// IO
func main() {
	args := extractProgramArgs(args{ConfigurationPath: "conf.yaml"})
	conf, err := LoadConfiguration(args.ConfigurationPath)
	if err != nil {
		log.Fatalf("bad configuration: %v", err)
	}

	log.Printf("loaded configuration: %v", conf)

	pipeline, err := buildPipeline(conf)
	if err != nil {
		log.Fatalf("unable to build the pipeline: %v", err)
	}

	err = pipeline.Process(conf.Input.Path, conf.Output.Path)

	if err != nil {
		log.Fatalf("pipeline ended in error, %v.", err)
	} else {
		log.Println("Finished")
	}
}

// TODO: move to lib
func buildPipeline(conf config) (pipe processing.FileProcessor, err error) {
	pipe = processing.CsvFileProcessor{
		ContentProc: processing.CsvContentProcessor{
			HeaderProcessor: processing.CsvHeaderProcessor{
				InputSch:  conf.Input.Schema,
				OutputSch: conf.Output.Schema,
			},
			BodyProcessor: processing.CsvBodyProcessor{
				RowProc: processing.CsvRowProcessor{
					PuBuilder: processing.StdProcessedUnitBuilder{
						UnitBuilder: processing.StdUnitBuilder{
							Sch:       conf.Input.Schema,
							BuildUnit: processing.NewStringValuesUnit,
						},
						BuildProcessingUnit: processing.NewImmutableProcessingUnit,
					}.Build,
					PuUnbuilder: processing.StdProcessedUnitUnBuilder{
						UnitUnBuilder: processing.StdUnitUnBuilder{
							Sch:                      conf.Output.Schema,
							FailOnMissingOutputField: false,
							DefaultRowContent:        "notMapped",
						},
					}.UnBuild,
					UnitProc: processing.CsvUnitProcessor{
						R: conf.Mapping,
					},
				},
			},
		},
		InputReader:  csv.NewReader(conf.Input.Schema),
		OutputWriter: csv.NewWriter(conf.Output.Schema),
		ErrorHandler: &processing.FileLogger{
			Path: conf.Logs.RowProcessingError.Path +
				conf.Logs.RowProcessingError.Prefix +
				conf.Name +
				conf.Logs.RowProcessingError.Suffix +
				".log",
		},
	}
	return
}
