#!/usr/bin/env bash
set -o errexit
set -o pipefail
set -o nounset

###################
# COMMON VARIABLE #
###################

SCRIPT_NAME=$(basename "$0")
SCRIPT_PATH=$(dirname "$0")

#########
# USAGE #
#########

function usage() {
	echo "Will replace go files package name in the directory without recursion."
	echo ""
	echo "Usage: ${SCRIPT_NAME} [OPTION]"
	echo "Mandatory options:"
	echo "-n: new package name (ex: 'titi')"
	echo "-p: package name (ex: 'toto')"
	echo "-t: files location (ex: './')"
	echo "Available options:"
	echo "-d: Print commands as they are executed."
	echo "-h: Help. Print usage."
	echo "-v: Print commands as they are executed without looking interpreting variables content."
	echo 
}

####################
# OPTIONS HANDLING #
####################

while getopts "dhn:p:t:v" option
do
	case $option in
			
		d)
			set -o xtrace
			;;
		h)
			usage
			exit 0
			;;
    n)
      NEW_PACKAGE_NAME=${OPTARG:-}
      ;;
		p)
      PACKAGE_NAME=${OPTARG:-}
      ;;
    t)
      TARGET_PATH=${OPTARG:-}
      ;;
    v)
			set -o verbose
			;;
		\?)
			usage
			exit 1
			;;
			
	esac
done

function unspecified_mandatory_option() {
	unspecified_mandatory_option_OPTION=$1
	echo "Mandatory option unspecified: -${unspecified_mandatory_option_OPTION}." 1>&2
	MANDATORY_OPTIONS_STATUS="KO"
}

if [ -z ${NEW_PACKAGE_NAME:-} ]; then
	unspecified_mandatory_option "-n"
fi
if [ -z ${PACKAGE_NAME:-} ]; then
	unspecified_mandatory_option "-p"
fi
if [ -z ${TARGET_PATH:-} ]; then
	unspecified_mandatory_option "-t"
fi

if [ -n "${MANDATORY_OPTIONS_STATUS:-}" ]; then
	usage
	exit 1
fi

##########################
# CONSTANTES DEFINITIONS #
##########################

PACKAGE="package "

###################
# INPUT CONSTANTE #
###################

FIND="${PACKAGE}${PACKAGE_NAME}"
REPLACE_BY="${PACKAGE}${NEW_PACKAGE_NAME}"

######################
# EXECUTION HANDLING #
######################

find ${TARGET_PATH} -maxdepth 1 -type f -exec sed -i "s/${FIND}/${REPLACE_BY}/g" {} \;
